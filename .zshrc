# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
setopt appendhistory autocd extendedglob nomatch notify
unsetopt beep
bindkey -e
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/bykow/.zshrc'

autoload -U compinit && compinit
# End of lines added by compinstall

# Path to your oh-my-zsh installation.
ZSH=/usr/share/oh-my-zsh/
source $ZSH/oh-my-zsh.sh
export STARSHIP_CONFIG=$HOME/.config/starship/starship.toml

# Prompt
eval "$(starship init zsh)"

# Allow Ctrl-z to toggle between suspend and resume
function Resume {
  fg
  zle push-input
  BUFFER=""
  zle accept-line
}
zle -N Resume
bindkey "^Z" Resume

mesaclear() {
    rm -rf ~/.cache/mesa_shader_cache
    rm -rf ~/.local/share/Steam/steam/steamapps/shadercache
}

# Uncomment the following line to display red dots whilst waiting for completion.
COMPLETION_WAITING_DOTS="true"

# PATH
PATH=/home/bykow/.local/bin:/home/bykow/.cargo/bin:$PATH
export PATH="$PATH:$HOME/.config/zide/bin"

# FZF
export FZF_DEFAULT_COMMAND='ag -g ""'
export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"
export FZF_COMPLETION_OPTS='-i'
source /usr/share/fzf/completion.zsh
source /usr/share/fzf/key-bindings.zsh

# Editor
export EDITOR=/usr/bin/helix

# ALIAS
alias vim="nvim"
alias docker="podman"
alias lg="lazygit"
alias backup="python3 /home/bykow/dotfile/scripts/bak/run.py"
alias task="go-task"
alias r="task -d /home/bykow/workspace/damocles/ cluster:reconcile"
alias hx="helix"
alias zhx="zellij -l welcome --config-dir ~/.config/yazelix/zellij options --layout-dir ~/.config/yazelix/zellij/layouts"
alias y="yazi"
alias l="eza -1la"
alias lt="eza -1laTL 2"
alias cat="bat"

alias pullall="for repo in *; do pushd \$repo && git pull && popd;done"

alias wgn="sudo systemctl restart wg-quick@wg-nym"
alias wgd="sudo systemctl restart wg-quick@wg-dc"
alias wgr="sudo systemctl restart wg-quick@wg-rack"

alias k="KUBECONFIG=/home/bykow/.kube/damocles.conf kubectl"
alias k9s="k9s --kubeconfig /home/bykow/.kube/damocles.conf"
alias k0="KUBECONFIG=/home/bykow/.kube/k8s-0.conf kubectl"
alias k0s="k9s --kubeconfig /home/bykow/.kube/k8s-0.conf"
alias k1="KUBECONFIG=~/.kube/k8s-1.conf kubectl"
alias k1s="k9s --kubeconfig /home/bykow/.kube/k8s-1.conf"

eval $(thefuck --alias)
eval $(keychain -q --eval)
eval "$(zoxide init zsh)"

export SOPS_AGE_KEY_FILE=~/.config/sops/age/keys.txt
export PATH="${KREW_ROOT:-$HOME/.krew}/bin:$PATH"
