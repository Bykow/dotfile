import os
import subprocess
from getpass import getpass
from subprocess import Popen, PIPE, STDOUT
from datetime import datetime

pass_cmd = "/usr/bin/zenity --password"
p0 = Popen([pass_cmd], shell=True, stdout=PIPE)
password = p0.stdout.read().decode('utf-8')

signin = "/usr/bin/op signin --raw"
p1 = Popen([signin], shell=True,
           stdin=PIPE, stdout=PIPE)
session = p1.communicate(password.encode('utf-8'))[0].decode()

today = datetime.now()
iso_date = today.isoformat()
p2 = os.system(
    'systemd-run --user -u borg-backup-'+iso_date+' /home/bykow/dotfile/scripts/bak/borg.sh '+session)

p3 = os.system("journalctl --user -feu borg-backup-"+iso_date)

exit()
