#!/bin/sh

export BORG_REPO=/mnt/factarr
export BORG_PASSCOMMAND='op item get vjkg6lgwk3cgmvhifeoc6ahmtm --fields password --reveal --session '$1

borg create\
    -v\
    --info\
    --stats\
    --show-rc\
    --progress\
    --compression lz4\
    --exclude-caches\
    --exclude '**/.cache/*'\
    --exclude '**/go/*'\
    --exclude '**/.gopath/*'\
    --exclude '**/.minikube/*'\
    --exclude '**/.local/share/Steam/*'\
    --exclude '**/Games/*'\
    --exclude '**/target/*'\
    --exclude '**/node_modules/*'\
    --exclude '**/containers/storage/*'\
    ::'{hostname}-{now}'\
    /home

borg prune\
    -v\
    --list\
    --info\
    --stats\
    --glob-archives '{hostname}-*'\
    --show-rc\
    --keep-daily 7\
    --keep-weekly 8\
    --keep-monthly 24
