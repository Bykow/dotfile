cryptsetup luksOpen /dev/nvme0n1p3 crypto
mount -o subvol=root /dev/mapper/crypto /mnt
mount /dev/nvme0n1p1 /mnt/boot
arch-chroot /mnt
mkinitcpio -P
