# Bykow Dotfiles

This repo contains all sorts of dotfile, config and scripts for various applications that I use to manage my desktop environment.

## Install list
### Paru
```
sudo pacman -S --needed base-devel git
git clone https://aur.archlinux.org/paru.git
cd paru
makepkg -si
sudo cp -frp scripts/pacman/pacman.conf /etc/pacman.conf
sudo cp -frp scripts/pacman/paru.conf /etc/paru.conf
```

### Build Speed
```
echo -n 'MAKEFLAGS="--jobs=$(nproc)"\n' | sudo tee -a /etc/makepkg.conf
```

### Basics
```
paru -S zsh alacritty starship swappy swaync wofi sway-audio-idle-inhibit-git swaylock-effects-git thefuck zoxide fzf waybar firefox grim slurp wl-clipboard nemo 1password 1password-cli greetd oh-my-zsh-git wdisplays ripgrep bottom lxappearance breeze-icons fwupd
```

### TTF & GTK
```
paru -S ttf-dejavu ttf-jetbrains-mono ttf-jetbrains-mono-nerd ttf-noto-nerd colloid-catppuccin-gtk-theme
```

### Wifi laptop
```
paru -S networkmanager
sudo systemctl enable NetworkManager.service
```

### Bluetooth
```
paru -S blueman
sudo systemctl enable bluetooth.service
```

### Greetd
```
sudo systemctl enable greetd.service
sudo cp -frp scripts/greeter/greetd-config.toml /etc/greetd/config.toml
sudo cp -frp scripts/greeter/start-sway /usr/local/bin/start-sway
```

### Misc
```
paru -S nextcloud-client obsidian zulip-desktop telegram-desktop satty grim wl-clipboard
```
