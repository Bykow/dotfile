#!/usr/bin/env bash

op=$( echo -e " Lock\n Poweroff\n Reboot\n Suspend\n Logout" | wofi -i --dmenu -W 1000 | awk '{print tolower($2)}' )

case $op in
  poweroff)
    /usr/local/bin/tvshutdown.sh
	  systemctl poweroff
    ;;
  reboot)
	  systemctl reboot
    ;;
  suspend)
	  swaylock --image ~/.config/sway/cat-sound.png --ignore-empty-password --daemonize --indicator-caps-lock --indicator --clock --timestr "%H:%M:%S" --datestr "%d %b %Y" --show-failed-attempts --indicator-idle-visible --fade-in 0.5 --effect-blur 3x3 --effect-pixelate 2
    /usr/local/bin/tvshutdown.sh
    systemctl suspend
    ;;
  lock)
	  swaylock --image ~/.config/sway/cat-sound.png --ignore-empty-password --daemonize --indicator-caps-lock --indicator --clock --timestr "%H:%M:%S" --datestr "%d %b %Y" --show-failed-attempts --indicator-idle-visible --fade-in 0.5 --effect-blur 3x3 --effect-pixelate 2
    ;;
  logout)
    swaymsg exit
    ;;
esac
