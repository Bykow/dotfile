#!/bin/sh

case "$1" in
--toggle)
    if [ "$(systemctl --user  check fishnet.service)" = "active" ]; then
        systemctl --user stop fishnet.service
    else
        systemctl --user start fishnet.service
    fi
    ;;
--icon)
    if [ "$(fishnet_status)" = "1" ]; then
        echo 
    else
        echo 
    fi
    ;;
*)
    echo "$(systemctl --user check fishnet.service)"
    # echo "$(cat /home/bykow/.fishnet-stats | jq '.total_batches')"
    ;;
esac
